package com.example;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class WSController {
	 
	@MessageMapping("/wsExample")
    @SendTo("/topic/firstExample")
    public SendMessageToClient serverClientCommunication(SendMessageToServer message) throws Exception {
        return new SendMessageToClient("Hello, " + message.getMessage());
    }
}
