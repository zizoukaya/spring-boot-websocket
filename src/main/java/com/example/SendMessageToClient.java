package com.example;

public class SendMessageToClient {

    private String content;

    public SendMessageToClient(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

}
